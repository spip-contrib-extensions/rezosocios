# Plugin rezosocios

Ce fichier sert de changelog, il liste les modifications lors du changement de version.

## 2.1.1 - 2024-09-13

- Ajouté reseau social LinkedIn Showcase

## 2.0.0 - 2023-06-09

### Changed

- Compatible SPIP 4.2
- SPIP 4.1 minimum
- Compatibilité PHP 7.4 à 8.2
- Icones SVG


## Version 1.0.x

* Compatibilité SPIP 3.2+
* Ajout d'un modèle et d'une inclusion générique : `inclure/rezosocio.html`
* Ajout de socicon (suppression des anciennes icônes png et de la fonction de logo)
* Ajout d'une option de configuration pour activer l'habillage
* Ajout d'une entrée de menu
* Ajout de 2 saisies pour sélectionner un lien et un type de réseau
* Ajout d'une noisette
* PSR, phpdoc

## Version 0.4.x

### Version 0.4.10 (2017-06-29)

- Ajout de liens vers les hashtag twitter

### Version 0.4.9 (2017-04-11)

* Réparation du formulaire d'association qui plantait

### Version 0.4.8 (2016-10-20)

* Séparation de Youtube en deux :
  * comptes utilisateur
  * chaînes

### Version 0.4.7

* Optimisation des images
* PSR SPIP

### Version 0.4.6

* Doubler le réseau social Linkedin par "Linkedin company" pour pouvoir avoir des liens directs vers les pages entreprises

### Version 0.4.5

* Résolution d'un bug  pour vk

### Version 0.4.4

* Ajout des réseaux sociaux suivants :
  * [instagram](https://www.instagram.com/)
  * [tumblr.](https://www.tumblr.com/)
  * [vimeo](https://vimeo.com/)

### Version 0.4.3

* Autoriser les rédacteurs à créer et modifier les réseaux sociaux
* Indentation et autres bricoles de code

### Version 0.4.2

* Première version utilisable
