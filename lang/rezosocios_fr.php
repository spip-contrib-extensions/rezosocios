<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rezosocios.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_activer_habillage_explication' => 'Substitue les titres par des icônes dans les entrées (modèle, menu, noisette). Celles-ci sont basées sur la police Socicon.',
	'cfg_activer_habillage_label' => 'Habillage',
	'cfg_activer_habillage_label_case' => 'Activer l’habillage sur le site public.',
	'cfg_apercu_habillage' => 'Aperçu :',
	'cfg_titre_rezosocios' => 'Configuration de "Réseaux sociaux"',

	// E
	'erreur_url_utilisee' => 'Le lien du profil de réseau social est déjà utilisé.',

	// I
	'icone_creation_rezosocio' => 'Créer un lien de réseau social',
	'icone_rezosocios' => 'Réseaux sociaux',
	'icone_supprimer_rezosocio' => 'Supprimer ce réseau social',
	'info_langue' => 'Langue du profil',
	'info_modifier_rezosocio' => 'Modifier le lien de réseau social',
	'info_nom_compte' => 'Nom du compte',
	'info_nom_compte_explication' => 'Indiquez uniquement l’identifiant du compte, pas l’URL complète. Exemple : pour https://twitter.com/Spip, indiquez « Spip »',
	'info_recherche_rezosocio_zero' => 'Aucun résultat dans les réseaux sociaux pour la recherche "@cherche_rezosocio@"',
	'info_retirer_rezosocio' => 'Retirer ce lien de réseau social',
	'info_retirer_rezosocios' => 'Retirer tous les réseaux sociaux',
	'info_type_rezosocios' => 'Type de réseau social',
	'info_url_site' => 'Lien du profil',

	// L
	'label_activer_rezosocio_objets' => 'Activer les liens de réseaux sociaux pour les contenus :',
	'lien_ajouter_rezosocio' => 'Ajouter ce lien de réseau social',

	// M
	'menu_description' => 'Vous pouvez avoir un ou plusieurs liens dans l’entrée de menu',
	'menu_id_rezosocio_label' => 'Un ou plusieurs numéros séparés par des virgules.',
	'menu_titre' => 'Liens vers les réseaux sociaux',

	// N
	'noisette_contenu_label' => 'Contenu',
	'noisette_id_rezosocio_label' => 'Sélection',
	'noisette_liaison_explication' => 'Prendre les réseaux liés à un contenu ?',
	'noisette_liaison_label' => 'Liaison',
	'noisette_liaison_non' => 'Non',
	'noisette_liaison_objet_contexte' => 'Le contenu du contexte',
	'noisette_liaison_objet_lie' => 'Un contenu choisi manuellement',
	'noisette_objet_lie_explication' => 'Raccourci d’un contenu : article10, rubrique5, etc. Avec le plugin Sélecteur générique, tapez les 1ères lettres d’un titre pour obtenir des suggestions.',
	'noisette_objet_lie_label' => 'Contenu lié',
	'noisette_rezosocios_description' => 'Barre de liens vers les réseaux sociaux, que l’on peut filtrer selon plusieurs critères.',
	'noisette_rezosocios_nom' => 'Liens vers les réseaux sociaux',

	// R
	'rezosocio' => 'Réseau social',
	'rezosocio_ajouter' => 'Ajouter un lien de réseau social',
	'rezosocio_aucun' => 'Aucun lien de réseau social',
	'rezosocio_creer_associer' => 'Créer et associer un lien de réseau social',
	'rezosocio_editer' => 'Modifier ce lien de réseau social',
	'rezosocio_logo' => 'Logo du lien de réseau social',
	'rezosocio_un' => '1 lien de réseau social',
	'rezosocios' => 'Réseaux sociaux',
	'rezosocios_nb' => '@nb@ liens de réseaux sociaux',

	// S
	'saisie_rezosocio_description' => 'Un ou plusieurs liens de réseaux sociaux',
	'saisie_rezosocio_option_multiple_label' => 'Choix multiples',
	'saisie_rezosocio_option_multiple_label_case' => 'Plusieurs choix possibles',
	'saisie_rezosocio_option_type_rezo_label' => 'Type de réseau',
	'saisie_rezosocio_titre' => 'Sélection de liens de réseaux sociaux',

	// T
	'texte_nouveau_rezosocio' => 'Nouveau lien de réseau social',
	'texte_rezosocio_statut' => 'Ce lien de réseau social est :',
	'titre_ajouter_un_rezosocio' => 'Ajouter un lien de réseau social',
	'titre_objets_lies_rezosocio' => 'Objets liés à ce réseau social',
	'titre_page_rezosocios_page' => 'Les liens de réseaux sociaux',
	'titre_tweetsde' => 'Tweets de @@name@',
	'twitter_hashtag' => 'Twitter (hashtag)',

	// Y
	'youtube_channel' => 'Youtube (chaîne)',
	'youtube_user' => 'Youtube (compte utilisateur)'
);
